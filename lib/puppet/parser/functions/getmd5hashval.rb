require 'digest'
module Puppet::Parser::Functions
  newfunction(:getmd5hashval, :type => :rvalue) do |args|
    Digest::MD5.hexdigest(lookupvar('adminpass'))
  end
end