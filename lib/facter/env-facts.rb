Facter.add('admin_key_buffer_size_mb') do
  setcode do
    Facter::Core::Execution.exec('echo $((`grep -i MemTotal /proc/meminfo | grep -o \'[[:digit:]]\+\'` / 4 / 1024))')
  end
end
