node default {

    $dbpass = 'openmeetings'

    postgresql::server::config_entry {
        'shared_buffers': value => "${admin_key_buffer_size_mb}MB",
    }

    postgresql::server::config_entry {
        'effective_cache_size': value => "${admin_key_buffer_size_mb}MB",
    }

    class { 'postgresql::server':
        ip_mask_deny_postgres_user => '0.0.0.0/32',
        ip_mask_allow_all_users    => '0.0.0.0/0',
        listen_addresses           => '*',
        postgres_password          => $dbpass,
    }
    ->
    file { "/root/.pgpass":
        owner => "root",
        group => "root",
        mode => 600,
        content => template('openmeetings/pgpass.erb')
    }
    ->
    class { 'openmeetings': dbpass => $dbpass }
}
