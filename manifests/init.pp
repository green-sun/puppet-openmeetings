class openmeetings (
        $dbpass = 'openmeetings',
        $adminpass = 'chickensandwich',
        $ssl = false,
    ) {

    Package { ensure => "latest" }

    case $::operatingsystem {
        'centos', 'redhat', 'fedora', 'scientific': {

        }
        'ubuntu', 'debian': {
            case $::lsbmajdistrelease {
                '12.04','14.04': {
                    $openmeetings_server_packages = [ "imagemagick", "unzip", "ffmpeg", "sox", "libgif-dev", "libavcodec-extra-53", "libfreetype6-dev", "libfreetype6", "xpdf", "libreoffice-writer", "libreoffice-calc", "libreoffice-impress", "libreoffice-draw", "libreoffice-math", "libreoffice-java-common", "swftools" ]
                 }
                '15.04', '16.04': {
                    $openmeetings_server_packages = [ "imagemagick", "unzip", "ffmpeg", "sox", "libgif-dev", "libav-tools", "libfreetype6-dev", "libfreetype6", "xpdf", "libreoffice-writer", "libreoffice-calc", "libreoffice-impress", "libreoffice-draw", "libreoffice-math", "libreoffice-java-common", "swftools" ]
                }
            }
        }
        'freebsd': {

        }
        'darwin': {

        }
        'default': {

        }
    }

    package { $openmeetings_server_packages: }
    ->
    exec { "ensure_javadir_created":
        command => "bash -c 'mkdir -p /usr/lib/jvm'",
        path    => "/usr/local/bin/:/usr/bin/:/usr/sbin/:/bin/",
        timeout => 0
    }
    ->
    file { "/usr/lib/jvm/jdk-8u131-linux-x64.tar.gz":
        owner => "root",
        group => "root",
        mode => 500,
        source => "puppet:///modules/openmeetings/jdk-8u131-linux-x64.tar.gz"
    }
    ->
    exec { "install_jdk":
        command => "bash -c 'cd /usr/lib/jvm && tar -xvzf jdk-8u131-linux-x64.tar.gz && sudo update-alternatives --install \"/usr/bin/java\" \"java\" \"/usr/lib/jvm/jdk1.8.0_131/bin/java\" 1 && sudo update-alternatives --install \"/usr/bin/javac\" \"javac\" \"/usr/lib/jvm/jdk1.8.0_131/bin/javac\" 1 && sudo update-alternatives --install \"/usr/bin/keytool\" \"keytool\" \"/usr/lib/jvm/jdk1.8.0_131/bin/keytool\" 1'",
        path    => "/usr/local/bin/:/usr/bin/:/usr/sbin/:/bin/",
        timeout => 0
    }
    ->
    exec { "ensure_openmeetings_created":
        command => "bash -c 'mkdir -p /opt/openmeetings'",
        path    => "/usr/local/bin/:/usr/bin/:/usr/sbin/:/bin/",
        timeout => 0
    }
    ->
    file { "/opt/openmeetings/apache-openmeetings-3.2.1.tar.gz":
        owner => "root",
        group => "root",
        mode => 400,
        source => "puppet:///modules/openmeetings/apache-openmeetings-3.2.1.tar.gz"
    }
    ->
    exec { "unzip_openmeetings":
        command => "bash -c 'cd /opt/openmeetings && tar -xvzf apache-openmeetings-3.2.1.tar.gz'",
        path    => "/usr/local/bin/:/usr/bin/:/usr/sbin/:/bin/",
        timeout => 0
    }
    ->
    postgresql::server::db { 'openmeetings':
        user     => 'openmeetings',
        password => postgresql_password('openmeetings', $dbpass),
    }
    ->
    exec { "update_install_openmeetings_postgres":
        command => join(["bash -c echo \"update om_user set password = '", getmd5hashval(), "' where id = 1;\" | sudo su - postgres -c 'psql -d openmeetings' ; cd /opt/openmeetings && ./admin.sh -i -v -tz America/Denver -email root@localhost.localdomain -group webbase -user admin --smtp-server localhost --db-type postgresql --db-name openmeetings --db-pass $dbpass --email-auth-user root@localhost.localdomain --password $adminpass --system-email-address root@localhost.localdomain --db-user openmeetings --db-host localhost --email-auth-pass $adminpass"], ""),
        path    => "/usr/local/bin/:/usr/bin/:/usr/sbin/:/bin/",
        timeout => 0,
        logoutput => true,
    }
    ->
    exec { "set_openmeetings_perms":
        command => "bash -c 'chmod 600 /opt/openmeetings/webapps/openmeetings/WEB-INF/classes/META-INF/persistence.xml'",
        path    => "/usr/local/bin/:/usr/bin/:/usr/sbin/:/bin/",
        timeout => 0,
    }

    if $ssl {
        file { "/opt/openmeetings/conf/jee-container.xml":
            owner => "root",
            group => "root",
            mode => 400,
            source => "puppet:///modules/openmeetings/ssl/jee-container.xml",
            require => Exec['set_openmeetings_perms']
        }
        ->
        file { "/opt/openmeetings/webapps/openmeetings/public/config.xml":
            owner => "root",
            group => "root",
            mode => 400,
            source => "puppet:///modules/openmeetings/ssl/config.xml"
        }
        ->
        file { "/opt/openmeetings/conf/red5.properties":
            owner => "root",
            group => "root",
            mode => 400,
            source => "puppet:///modules/openmeetings/ssl/red5.properties"
        }
        ->
        file { "/opt/openmeetings/conf/red5-core.xml":
            owner => "root",
            group => "root",
            mode => 400,
            source => "puppet:///modules/openmeetings/ssl/red5-core.xml"
        }
    }
}
