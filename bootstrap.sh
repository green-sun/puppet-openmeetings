#!/bin/bash

# Update package repository before installing
apt-get update

# Install dependencies for running puppet/cloning code.
apt-get install -y puppet

# Add all dependencies for main puppet module.
puppet module install puppetlabs-postgresql