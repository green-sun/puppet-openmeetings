# GreenSun OpenMeetings puppet module
This is the GreenSun puppet module for installing/configuring [OpenMeetings](https://openmeetings.apache.org/).  The plugin makes use of dependencies [puppetlabs-postgresql](https://github.com/puppetlabs/puppetlabs-postgresql) to install the [PostgreSQL database](https://www.postgresql.org/), and then copies all required configuration files into place and installs at /opt/openmeetings.

The only requirement is to have [Vagrant](https://www.vagrantup.com/) installed, and the included Vagrantfile will work to do the following on running `vagrant up --provision`:
* Provision a VM based off the community public release of [Ubuntu Server 16.04](https://www.ubuntu.com/download/server), which can be updated anytime from within your Vagrant root directory on your host machine by running `vagrant box update`
* Install a PostgreSQL database with a couple of optimizations to improve performance, noting the authors are no experts in PostgreSQL tuning - improvements to the default configuration are welcomed.
* Install OpenMeetings according to the [manifests/default.pp](manifests/default.pp) file at http://192.168.222.44:5080/ once the server is started with `cd /opt/openmeetings && ./red5.sh`

Support for writing the alternate configuration files to enable RTMPS/HTTPS is available with the `ssl => true` parameter, with the understanding that I can't pre-generate your keystore for you.  That's fun - read the docs and ensure you keystore is copied to /opt/openmeetings/keystore.jmx and /opt/openmeetings/keystore.screen.jmx and it should work.
